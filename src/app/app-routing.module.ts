import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const routes: Routes = [
  { path: 'login',   loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule) },
  { path: 'register', loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule) },
  { path: 'settings', loadChildren: () => import('./pages/settings/settings.module').then( m => m.SettingsPageModule) },
  { path: 'edit-profile', loadChildren: () => import('./pages/profile/profile.module').then( m => m.ProfilePageModule) },
  {path: 'trainings',loadChildren: () => import('./pages/training/training.module').then( m => m.TrainingPageModule)},
  {path: 'exercices',loadChildren: () => import('./pages/exercices/exercices.module').then( m => m.ExercicesPageModule)},
  {path: 'videochallenges',loadChildren: () => import('./pages/videochallenges/videochallenges.module').then( m => m.VideochallengesPageModule)},
  {
    path: '',
    loadChildren: () => import('./pages/programsgroup/programsgroup.module').then( m => m.ProgramsgroupPageModule)
  },
  {
    path: 'programs',
    loadChildren: () => import('./pages/programsgroup/programsgroup.module').then( m => m.ProgramsgroupPageModule)
  },
  {
    path: 'videochallengelist',
    loadChildren: () => import('./pages/videochallengelist/videochallengelist.module').then( m => m.VideochallengelistPageModule)
  },
  {
    path: 'seances',
    loadChildren: () => import('./pages/seance/seance.module').then( m => m.SeancePageModule)
  },
  {
    path: 'resetpassword',
    loadChildren: () => import('./pages/resetpassword/resetpassword.module').then( m => m.ResetpasswordPageModule)
  },
  {
    path: 'payement',
    loadChildren: () => import('./pages/payement/payement.module').then( m => m.PayementPageModule)
  },
  {
    path: 'rules',
    loadChildren: () => import('./pages/rules/rules.module').then( m => m.RulesPageModule)
  },
  {
    path: 'emailrestpassword',
    loadChildren: () => import('./pages/emailrestpassword/emailrestpassword.module').then( m => m.EmailrestpasswordPageModule)
  },
  {
    path: 'personalizetraining',
    loadChildren: () => import('./pages/personalizetraining/personalizetraining.module').then( m => m.PersonalizetrainingPageModule)
  },
  {
    path: 'programs1',
    loadChildren: () => import('./pages/programs/programs.module').then( m => m.ProgramsPageModule)
  },
  {
    path: 'detailsprogram',
    loadChildren: () => import('./pages/detailsprogram/detailsprogram.module').then( m => m.DetailsprogramPageModule)
  },
  {
    path: 'detailsseance',
    loadChildren: () => import('./pages/detailsseance/detailsseance.module').then( m => m.DetailsseancePageModule)
  },
  {
    path: 'detailsexercice',
    loadChildren: () => import('./pages/detailsexercice/detailsexercice.module').then( m => m.DetailsexercicePageModule)
  },
  {
    path: 'detailstraining',
    loadChildren: () => import('./pages/detailstraining/detailstraining.module').then( m => m.DetailstrainingPageModule)
  },
  
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
