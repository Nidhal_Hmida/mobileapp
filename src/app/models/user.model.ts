export class  User{
    id : string
    fullname : string
    birthday : string
    country : string
    length : string
    weight : string
    position : string
    image : string
    email : string
    password : string
    stateCompte : string
    statePayment : string
  
    /*badges : Badge[]*/

    constructor( id : string,fullname : string,country:string,length:string,weight:string,email:string,password:string,birthday : string,prefrence : string,image : string)
    {
      this.id = id
      this.fullname = fullname
      this.email = email
      this.country = country
      this.length = length
      this.weight = weight
      this.password = password
      this.birthday = birthday
      this.position = prefrence
      this.image = image
      this.stateCompte = 'Non activée'
      this.statePayment = "Non payé"
    }

}