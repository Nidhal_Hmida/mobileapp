export class VideoChallenge
{
    id : string 
    idCompte : string
    seance : string
    badge : string 
    points : string
    socialMedia : boolean
    title : string
    path : string
    observation : string
    
}