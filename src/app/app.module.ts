import { LOCALE_ID, NgModule } from '@angular/core';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr);
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClient, HttpClientModule} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Camera } from '@ionic-native/camera/ngx';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';


// Components
import { registerLocaleData } from '@angular/common';
import { MediaCapture } from '@ionic-native/media-capture/ngx'
import { File } from '@ionic-native/file/ngx';
import { GlobalsService } from './services/globals/globals.service';
import { CoachfootballService } from './services/coachfootballservice.service';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { StreamingMedia } from '@ionic-native/streaming-media/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslationService } from './services/translation.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';

import { DatePicker } from '@ionic-native/date-picker/ngx';

export function LanguageLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent
   ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (LanguageLoader),
        deps: [HttpClient]
      }
    }),
  ],

  providers: [
 
    StatusBar,
    SplashScreen,
    MediaCapture,
    Camera,
    GlobalsService,
    CoachfootballService,
    NativeStorage,
    File,
    DatePicker,
    FileTransfer,
    GooglePlus,
    Facebook,
    Deeplinks,
    TranslationService,
    StreamingMedia,  
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: LOCALE_ID, useValue: 'fr-FR'},
  ],
  bootstrap: [AppComponent]
})

export class AppModule {}
