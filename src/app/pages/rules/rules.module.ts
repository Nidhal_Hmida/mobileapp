import { NgModule } from '@angular/core';
import { RulesPageRoutingModule } from './rules-routing.module';
import { RulesPage } from './rules.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    RulesPageRoutingModule
  ],
  declarations: [RulesPage]
})
export class RulesPageModule {}
