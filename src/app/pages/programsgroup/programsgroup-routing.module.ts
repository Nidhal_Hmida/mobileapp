import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProgramsgroupPage } from './programsgroup.page';

const routes: Routes = [
  {
    path: '',
    component: ProgramsgroupPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProgramsgroupPageRoutingModule {}
