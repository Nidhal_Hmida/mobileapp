import { NgModule } from '@angular/core';
import { ProgramsgroupPageRoutingModule } from './programsgroup-routing.module';
import { ProgramsgroupPage } from './programsgroup.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    ProgramsgroupPageRoutingModule
  ],
  declarations: [ProgramsgroupPage]
})
export class ProgramsgroupPageModule {}
