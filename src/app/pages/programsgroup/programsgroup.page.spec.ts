import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProgramsgroupPage } from './programsgroup.page';

describe('ProgramsgroupPage', () => {
  let component: ProgramsgroupPage;
  let fixture: ComponentFixture<ProgramsgroupPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramsgroupPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProgramsgroupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
