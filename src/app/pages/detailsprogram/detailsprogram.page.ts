import { Component, OnInit } from '@angular/core';
import {  LoadingController, MenuController, NavController } from '@ionic/angular';
import { Seance } from 'src/app/models/seance.model';
import { Program } from '../../models/program.model';
import { CoachfootballService } from '../../services/coachfootballservice.service';
import { GlobalsService } from '../../services/globals/globals.service';

@Component({
  selector: 'app-detailsprogram',
  templateUrl: './detailsprogram.page.html',
  styleUrls: ['./detailsprogram.page.scss'],
})

export class DetailsprogramPage implements OnInit {

  program : Program
  constructor( public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private service :CoachfootballService,
    public global : GlobalsService) { 
     }

    ionViewWillEnter()
    { 
      this.program = this.service.currentProgram   
    }

  ngOnInit() {
    this.program = this.service.currentProgram   
  }

  getSeances()
  {
     this.service .getChild("/programs",this.program.id,"seances").subscribe((data: Seance[])=>{  
      this.service.serviceSeance(data)  
      this.navCtrl.navigateForward("seances")
       });  
  }
  

  back()
  {
    this.navCtrl.navigateBack('programs1')
  }

}
