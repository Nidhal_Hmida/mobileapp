import { NgModule } from '@angular/core';
import { ResetpasswordPageRoutingModule } from './resetpassword-routing.module';
import { ResetpasswordPage } from './resetpassword.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
  SharedModule,
  ResetpasswordPageRoutingModule
  ],
  declarations: [ResetpasswordPage,
  
  ]
})
export class ResetpasswordPageModule {}
