import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VideochallengelistPage } from './videochallengelist.page';

const routes: Routes = [
  {
    path: '',
    component: VideochallengelistPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VideochallengelistPageRoutingModule {}
