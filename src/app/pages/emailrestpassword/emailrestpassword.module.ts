import { NgModule } from '@angular/core';
import { EmailrestpasswordPageRoutingModule } from './emailrestpassword-routing.module';
import { EmailrestpasswordPage } from './emailrestpassword.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    EmailrestpasswordPageRoutingModule
  ],
  declarations: [EmailrestpasswordPage]
})
export class EmailrestpasswordPageModule {}
