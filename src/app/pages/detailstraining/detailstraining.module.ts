import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailstrainingPageRoutingModule } from './detailstraining-routing.module';

import { DetailstrainingPage } from './detailstraining.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailstrainingPageRoutingModule
  ],
  declarations: [DetailstrainingPage]
})
export class DetailstrainingPageModule {}
