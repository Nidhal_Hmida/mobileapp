import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { Exercise } from 'src/app/models/exercice.model';
import { Training } from 'src/app/models/training.model';
import { CoachfootballService } from 'src/app/services/coachfootballservice.service';

@Component({
  selector: 'app-detailstraining',
  templateUrl: './detailstraining.page.html',
  styleUrls: ['./detailstraining.page.scss'],
})
export class DetailstrainingPage implements OnInit {

  training : Training
 
  constructor(
    private navCtrl: NavController,
    public service: CoachfootballService,
    private alertController: AlertController,
    public loadingCtrl: LoadingController
    ) 
  { }

  back()
  {
    this.navCtrl.navigateBack("trainings")
  }

  ionViewWillEnter()
  { 
    this.training = this.service.currentTraining  
  }



ngOnInit() {
  this.training = this.service.currentTraining     
}


async presentAlertPrompt() {
  const alert = await this.alertController.create({
    cssClass: 'my-custom-alert',
    header: "Avis séance",
    inputs: [
      {
        name: 'Avis',
        id: 'paragraph',
        type: 'textarea',
        placeholder: 'Avis'
      } 
    ],
    buttons: [
      {
        text: 'Annuler',
        role: 'cancel',
        cssClass: 'secondary',
        handler: () => {
         
        }
      }, {
        text: 'Ok',
        handler: data  => {
         
          if(data["Avis"]!= "" && data["Avis"]!= " ")
          {
          this.opinion(data["Avis"])
          }
        }
        
      }
    ]
  });
  await alert.present();
}


async presentAlertPrompt2() {
  const alert = await this.alertController.create({
    cssClass: 'my-custom-alert',
    header: "Confirmation",
    message:"Êtes vous sur que vous voulez supprimer la séance",
    buttons: [
      {
        text: 'Annuler',
        role: 'cancel',
        cssClass: 'secondary',
        handler: () => {
         
        }
      }, {
        text: 'Ok',
        handler: data  => { 
        this.delete()
        }
        
      }
    ]
  });

  await alert.present();


}

  async opinion(avis)
{
  const loader = await this.loadingCtrl.create({duration :500});
  loader.present();
  var training = this.training
  training.opinion = avis
  training.startTime= training.startTime.replace('T',' ').substring(0,16)

  training.exercise = new Exercise(this.service.currentExercice.id)
  this.service.modifyTraining("trainings",training ).subscribe((data : Training)=>{

     if(data!= undefined)
     { 
      loader.onWillDismiss().then(() => {
        this.presentAlertConfirm('Confirmation!',"Merci pour votre avis")
       });
      this.training.opinion = data.opinion
      var indice = this.service.getData(this.training,this.service.trainings)
      this.service.trainings.splice(indice,1)
      this.service.trainings.splice(indice,0,this.training)
      this.service.serviceTraining(this.service.trainings)
     
        
     }
     else
     {
       loader.onWillDismiss().then(() => {
         this.presentAlertConfirm('Probléme!',"Modification non effectuée")
        });
        
     }
   
   });
}

  async delete()
{
  const loader = await this.loadingCtrl.create({duration :500});
  loader.present();
  this.service.deleteTraining("trainings",this.training).subscribe((data)=>{
    var indice = this.service.getData(this.training,this.service.trainings)
      this.service.trainings.splice(indice,1)
      this.service.serviceTraining(this.service.trainings)
      loader.onWillDismiss().then(() => {
        this.presentAlertConfirm('Confirmation!',"La séance est suppriméé")
       });
       if(this.service.trainings.length!=0)
       this.navCtrl.navigateForward("trainings")
       else
       this.navCtrl.navigateForward("detailsexercice")
      },
      (error) => {
        
      })
    
}


async presentAlertConfirm(header,message) {
  const alert = await this.alertController.create({
    cssClass: 'my-custom-class',
    header: header,
    message: message ,
    buttons: [
{
        text: 'Ok',
        handler: () => {
        }
      }
    ]
  });

  await alert.present();
}



}
