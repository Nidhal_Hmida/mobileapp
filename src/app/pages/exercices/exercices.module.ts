import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { ExercicesPageRoutingModule } from './exercices-routing.module';
import { ExercicesPage } from './exercices.page';

@NgModule({
  imports: [
    SharedModule,
    ExercicesPageRoutingModule
  ],
  declarations: [ExercicesPage]
})
export class ExercicesPageModule {}
