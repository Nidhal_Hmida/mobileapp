import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailsseancePage } from './detailsseance.page';

describe('DetailsseancePage', () => {
  let component: DetailsseancePage;
  let fixture: ComponentFixture<DetailsseancePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsseancePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailsseancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
