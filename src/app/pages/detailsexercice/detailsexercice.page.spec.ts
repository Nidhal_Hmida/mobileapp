import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailsexercicePage } from './detailsexercice.page';

describe('DetailsexercicePage', () => {
  let component: DetailsexercicePage;
  let fixture: ComponentFixture<DetailsexercicePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsexercicePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailsexercicePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
