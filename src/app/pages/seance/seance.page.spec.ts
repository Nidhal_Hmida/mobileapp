import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SeancePage } from './seance.page';

describe('SeancePage', () => {
  let component: SeancePage;
  let fixture: ComponentFixture<SeancePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeancePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SeancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
