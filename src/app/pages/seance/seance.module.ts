import { NgModule } from '@angular/core';
import { SeancePageRoutingModule } from './seance-routing.module';
import { SeancePage } from './seance.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
   SharedModule,
    SeancePageRoutingModule
  ],
  declarations: [SeancePage]
})
export class SeancePageModule {}
